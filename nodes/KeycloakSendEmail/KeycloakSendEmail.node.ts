import {
    IExecuteFunctions, INodeExecutionData,
    INodeType,
    INodeTypeDescription, NodeOperationError,
} from 'n8n-workflow';

/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */
export class KeycloakSendEmail implements INodeType {
    description: INodeTypeDescription = {
        displayName: 'Send Keycloak Email',
        name: 'keycloakSendEmail',
        group: ['input'],
        icon: 'file:KeycloakSendEmail.node.svg',
        version: 2,
        description: 'Send a reset password email in Keycloak',
        defaults: {
            name: 'Send Keycloak Email',

        },
        inputs: ['main'],
        outputs: ['main'],
        properties: [
            {
                displayName: 'User ID',
                name: 'userId',
                type: 'string',
                default: '',
                description: 'ID of the user to send the email to',
            },
            {
                displayName: 'Add Result to Output Field',
                name: 'outputField',
                type: 'string',
                default: '',
                description: 'Retreive for each row the result of email action',
            },
            {
                displayName: 'Realm',
                name: 'realm',
                type: 'string',
                default: '',
                description: 'Keycloak realm under which the user resides',
            },
            {
                displayName: 'Default Action',
                name: 'action',
                type: 'options',
                options: [
                    {
                        name: 'UPDATE_PASSWORD',
                        value: 'UPDATE_PASSWORD',
                    },
                    // Add other actions if available
                ],
                default: 'UPDATE_PASSWORD',
                description: 'The action by default to trigger in the email',
            },
            {
                displayName: 'Get Action Into Field of Each Row',
                name: 'inputFieldAction',
                type: 'string',
                default: '',
                description: 'For each row the action for email is get in this field',
            },
            {
                displayName: 'Active Token',
                name: 'activeToken',
                type: 'string',
                default: '',
                description: 'Token for auth Keycloak',
            },
        ],
        credentials: [
            {
                name: 'keycloakCredentialsApi',
                required: true,
            },
        ],
    };
    async execute(this: IExecuteFunctions): Promise<any> {
        const action:string = this.getNodeParameter('action', 0) as string;
        const outputField = this.getNodeParameter('outputField', 0) as string;
        const realm: string = this.getNodeParameter('realm', 0) as string;
        const activeToken: string = this.getNodeParameter('activeToken', 0) as string;
        const items = this.getInputData();
        const credentials = await this.getCredentials('keycloakCredentialsApi') as {
            serverUrl: string;
            apiToken: string;
            clientId?: string;
            clientSecret?: string;
            defaultRealm?: string;
        };
        let token: any = activeToken || "";
        if (!token && credentials.apiToken)
            token = credentials.apiToken;

        const userEndpoint = `${credentials.serverUrl}realms/${realm || credentials.defaultRealm}/protocol/openid-connect/token`;
        if (!token && credentials.clientId && credentials.clientSecret) {
            //get token
            const requestObj = {
                method: 'POST',
                uri: userEndpoint,
                body: `grant_type=client_credentials&client_id=${credentials.clientId}&client_secret=${credentials.clientSecret}`,
                json: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization':
                        'Basic ' +
                        Buffer.from(credentials.clientId + ':' + credentials.clientSecret).toString('base64')
                },
            }

            const responseData = await this.helpers.request(requestObj);

            token = responseData.access_token
        }
        if (!token) {
            throw new NodeOperationError(this.getNode(), 'token is empty.');
        }



        const headers = {
            Authorization: `Bearer ${token}`,
        };
        const output=[]
        for (let i = 0; i < items.length; i++) {
            const item: INodeExecutionData = items[i]
            const userId: string = this.getNodeParameter('userId', i) as string;

            const apiUrl: string = `${credentials.serverUrl}admin/realms/${realm || credentials.defaultRealm}/users/${userId}/execute-actions-email`;
            const inputFieldAction: string = this.getNodeParameter('inputFieldAction', i) as string;
            let activeAction: any= action;

            if(inputFieldAction !==""){
                activeAction= item.json[inputFieldAction] || action;
            }

            try {
                if(!action) {
                    if (outputField) {
                        item.json[outputField] = {result: false, message: 'Email not send', reason: "no action found"}
                    }
                }else{
                    const responseData = await this.helpers.request({
                        method: 'PUT',
                        uri: apiUrl,
                        body: [activeAction],
                        headers: headers,
                    });

                    if (outputField) {
                        item.json[outputField] = {result: true, message: 'Email send', data: responseData}
                    }
                }
                output.push(item)

            } catch (error) {
                if(outputField) {
                    item.json[outputField] = {result: false, message: 'Email not send', reason: error.message}
                }
                output.push(item)
            }


        }

        return [output]
    }
}
