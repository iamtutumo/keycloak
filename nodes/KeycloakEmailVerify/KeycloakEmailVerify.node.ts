import {
    INodeType,
    INodeTypeDescription,
    IExecuteFunctions,
} from 'n8n-workflow';

const ok=0,ko=1,error=2;

/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */
export class KeycloakEmailVerify implements INodeType {
    description: INodeTypeDescription = {
        displayName: 'Verify Keycloak Email',
        name: 'keycloakEmailVerify',
        icon: 'file:KeycloakEmailVerify.node.svg', // Provide a path to your icon
        group: ['input'],
        version: 2,
        description: 'Verify email existence in Keycloak',
        defaults: {
            name: 'Verify Keycloak Email',

        },
        inputs: ['main'],
        // eslint-disable-next-line n8n-nodes-base/node-class-description-outputs-wrong
        outputs: ['main','main','main'],
        outputNames: ['ok', 'ko', 'error'],
        credentials: [
            {
                name: 'keycloakCredentialsApi',
                required: true,
            },
        ],
        properties: [
            {
                displayName: 'Email',
                name: 'email',
                type: 'string',
                placeholder: 'name@email.com',
                default: '',
                description: 'The email to check in Keycloak',
            },
            {
                displayName: 'Realm',
                name: 'realm',
                type: 'string',
                default: '',
                description: 'The Keycloak realm under which the email check should be performed',
            },
            {
                displayName: 'Grant Type',
                name: 'grantType',
                type: 'options',
                options: [
                    {
                        name: 'Client Credentials',
                        value: 'client_credentials',
                    },
                    // Add other grant types as needed
                ],
                default: 'client_credentials',
                description: 'The OAuth 2.0 grant type for obtaining the token',
            },
            {
                displayName: 'Active Token',
                name: 'activeToken',
                type: 'string',

                default: '',
                description: 'Token for auth Keycloak',
            },
        ],
    };

    async execute(this: IExecuteFunctions): Promise<any> {
        let outputData: any = [[], [], []];


        const items = this.getInputData();
        const realm: string = this.getNodeParameter('realm', 0) as string;
        const activeToken: string = this.getNodeParameter('activeToken', 0) as string;
        const grantType = this.getNodeParameter('grantType', 0) as string;

        const credentials = await this.getCredentials('keycloakCredentialsApi') as {
            serverUrl: string;
            apiToken: string;
            clientId?: string;
            clientSecret?: string;
            defaultRealm?: string;
        };
        let token: any = activeToken || "";
        if(!token && credentials.apiToken)
            token = credentials.apiToken;

        const userEndpoint = `${credentials.serverUrl}realms/${realm || credentials.defaultRealm}/protocol/openid-connect/token`;
        if (!token && credentials.clientId && credentials.clientSecret) {
            //get token
            const requestObj = {
                method: 'POST',
                uri: userEndpoint,
                body: `grant_type=${grantType}&client_id=${credentials.clientId}&client_secret=${credentials.clientSecret}`,
                json: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization':
                        'Basic ' +
                        Buffer.from(credentials.clientId + ':' + credentials.clientSecret).toString('base64')
                },
            }

            const responseData = await this.helpers.request(requestObj);

            token = responseData.access_token
        }
        if (!token) {
            outputData[error].push({
                json: {result: false, message: 'Failed to obtain Keycloak token.'},
            })
            return outputData;
        }

        try {
            for (let i = 0; i < items.length; i++) {
                const item = items[i]
                const email = this.getNodeParameter('email', i) as string;
                const encodedSearch = encodeURIComponent(`${email}`);
                const apiUrl = `${credentials.serverUrl}admin/realms/${realm|| credentials.defaultRealm}/users?search=${encodedSearch}`;
                const headers = {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                };
                const requestObj = {
                    method: 'GET',
                    uri: apiUrl,
                    headers: headers,
                }

                const responseData = await this.helpers.request(requestObj);


                const data = JSON.parse(responseData);

                if (data && data.length > 0) {
                    item.json.result = data[0]
                    outputData[ok].push(item)

                } else {
                    item.json.result = data[0]
                    outputData[ko].push(item)
                }
            }
            return outputData;
        }catch(error){

            return [[], [], [{
                json: {result: false, message: error.message, items: items},
            }]];
        }
    }
}
