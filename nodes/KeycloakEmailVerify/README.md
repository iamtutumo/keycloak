# Keycloak Email Verify Node

This node provides functionality to verify if an email exists within a specified realm in Keycloak.

![Keycloak Email Verify Icon](KeycloakSendEmail.node.svg)

## Description

The `Keycloak Email Verify` node helps in verifying the existence of an email address in Keycloak. It can check the existence of an email address within a realm and can return one of three outputs - `ok`, `ko`, or `error`, indicating the success or failure of the email verification process.

## Configuration

### Inputs

- **Main Input**: This is where you provide the data for the email to be verified.

### Outputs

- **ok**: Indicates that the email exists within the specified Keycloak realm.
- **ko**: Indicates that the email does not exist within the specified Keycloak realm.
- **error**: Indicates an error in the verification process, such as authentication issues or server errors.

### Credentials

- **keycloakCredentialsApi**: These credentials are required for this node. They should include the Server URL, optionally an API Token, Client ID, Client Secret, and a default realm.

### Parameters

- **Email**: This is the email address you want to check in Keycloak.

- **Realm**: The Keycloak realm in which the verification should be performed. If this is left blank, the default realm provided in the credentials will be used.

- **Grant Type**: The OAuth 2.0 grant type used for obtaining the token. Currently, the only available option is `Client Credentials`, but more can be added if necessary.

- **Active Token**: A token for authenticating with Keycloak. If this is left blank and if the API Token is also not provided in the credentials, the node will try to obtain a token using the Client ID and Client Secret.

## Usage

1. Connect your email data source to the main input.
2. Provide the necessary parameters such as the email address, realm, grant type, and active token.
3. Provide the necessary Keycloak credentials or an Active Token.
4. Execute the node. Depending on whether the email exists in the realm, you will either receive an `ok`, `ko`, or `error` output.

## Example

Assuming you have a list of emails and you want to verify if a particular email, "user@example.com", exists in the Keycloak realm "myRealm":

1. Connect your email data source to the main input.
2. Set the `Email` parameter to "user@example.com".
3. Set the `Realm` parameter to "myRealm".
4. Choose `Client Credentials` as the Grant Type.
5. Provide the necessary Keycloak credentials or an Active Token.
6. Execute the node. If the email exists in "myRealm", you will get an `ok` output. If it doesn't, you will get a `ko` output. In case of any errors, the `error` output will be triggered.

Always ensure sensitive data, like tokens and credentials, are handled securely.
