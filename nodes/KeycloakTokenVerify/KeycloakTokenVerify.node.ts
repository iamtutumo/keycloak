import {
    IExecuteFunctions,
    INodeType,
    INodeTypeDescription,
    NodeOperationError
} from 'n8n-workflow';

const ok=0,ko=1,error=2;
function getValue(keyString: string,item: any): any{

    const keyArray :any =keyString.split('.');
    let mappedValue: any
    if('json' in item){
        const data= item['json'];
        mappedValue = {...data};
        let k :any;
        while( k = keyArray.shift()  )
        {
            if(typeof mappedValue == 'object' && k in mappedValue )
                mappedValue=mappedValue[k] ||''
            else
                mappedValue=''
        }
    }
    return mappedValue
}

/* eslint n8n-nodes-base/node-param-type-options-password-missing: "off" */

export class KeycloakTokenVerify implements INodeType {
    description: INodeTypeDescription = {
        displayName: 'Keycloak Token Verify',
        name: 'keycloakTokenVerify',
        icon: 'file:KeycloakTokenVerify.node.svg',
        group: ['transform'],
        version: 2,
        description: 'Verify token validity in Keycloak',
        defaults: {
            name: 'Keycloak Token Verify',

        },
        inputs: ['main'],
        // eslint-disable-next-line n8n-nodes-base/node-class-description-outputs-wrong
        outputs: ['main','main','main'],
        outputNames: ['ok', 'ko', 'error'],
        credentials: [{
            name: 'keycloakCredentialsApi',
            required: true,
        }],
        properties: [
            {
                displayName: 'Token Key',
                name: 'tokenKey',
                type: 'string',
                default: 'headers.authorization',
                description: 'The key for token to check in Keycloak. "Bearer" keyword is removed.',
            },
            {
                displayName: 'Realm',
                name: 'realm',
                type: 'string',
                default: '',
                description: 'The realm to check in Keycloak. If empty, realm get in Credentials.',
            },
        ],
    };

    async execute(this: IExecuteFunctions): Promise<any> {
        let outputData: any = [[], [], []];
        const credentials = await this.getCredentials('keycloakCredentialsApi');
        const realm: string = this.getNodeParameter('realm', 0) as string;
        const tokenKey: string = this.getNodeParameter('tokenKey', 0) as string;
        let token: string = "";
        const introspectionEndpoint = `${credentials.serverUrl}realms/${realm || credentials.defaultRealm}/protocol/openid-connect/token/introspect`;


        const items = this.getInputData();

        try {

            for (let i = 0; i < items.length; i++) {
                const item=items[i]
                token = token || getValue(tokenKey, item);
                token = token.replace('Bearer ', '')
                if (!token) {
                    throw new NodeOperationError(this.getNode(), 'token is empty.');
                }
                const requestBody = {
                    token
                };


                const responseData = await this.helpers.request({
                    method: 'POST',
                    uri: introspectionEndpoint,
                    body: requestBody,
                    json: true,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization':
                            'Basic ' +
                            Buffer.from(credentials.clientId + ':' + credentials.clientSecret).toString('base64')
                    },
                });

                if (responseData.active) {
                    item.json.result = {
                        result: true,
                        message: 'token is valid in Keycloak.',
                        response: responseData
                    };
                    outputData[ok].push(item)
                } else {
                    item.json.result = {
                        result: false,
                        message: 'token is invalid in Keycloak.',
                        response: responseData
                    };
                    outputData[ko].push(item)
                }

            }
            return outputData;
        } catch (err) {
            items[0].json.result = {result: false, message: err.message};
            outputData[error]=items
            return outputData;
        }

    }

}
