# Keycloak Send Email Node

This node allows you to send a reset password email to a user within Keycloak.

## Description

The `Keycloak Send Email` node facilitates sending emails to users, primarily for password reset actions, within a specified realm in Keycloak.

![Keycloak Send Email Icon](KeycloakSendEmail.node.svg)

## Configuration

### Inputs

- **Main Input**: This is where you provide the data for the user to whom the email needs to be sent.

### Outputs

- **Main Output**: Provides feedback on the success or failure of the email sending process.

### Credentials

- **keycloakCredentialsApi**: Credentials are required for this node. They should include the Server URL, optionally an API Token, Client ID, Client Secret, and a default realm.

### Parameters

- **User ID**: The ID of the user in Keycloak to whom you want to send the email.

- **Realm**: The Keycloak realm where the user is located. If left blank, the node will use the default realm provided in the credentials.

- **Action**: This parameter lets you choose the action that the email will trigger. Currently, the only available action is `UPDATE_PASSWORD`, which prompts the user to update their password.

- **Active Token**: A token for authenticating with Keycloak. If left blank and if the API Token is also not provided in the credentials, the node will try to obtain a token using the Client ID and Client Secret.

## Usage

1. Connect your data source to the main input of the node.
2. Set the User ID, Realm (or use the default from credentials), and choose the desired Action.
3. Optionally provide an Active Token or use the one from credentials.
4. Execute the node. If the email is sent successfully, you will receive feedback on the main output. If there's an error, it will be thrown by the node.

### Example

Let's say you have a list of users, and you want to send a password reset email to a specific user within the Keycloak realm "myRealm":

1. Connect your user data source to the main input.
2. Set the `User ID` parameter using the user's ID from your data source.
3. Set the `Realm` parameter to "myRealm".
4. Choose `UPDATE_PASSWORD` as the Action.
5. Provide the necessary Keycloak credentials or an Active Token.
6. Execute the node. If the email is sent successfully, you will get a positive response in the main output.

Remember to always handle sensitive data, such as tokens and credentials, with care.
