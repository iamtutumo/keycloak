import {
    ICredentialType,
    INodeProperties,
} from 'n8n-workflow';

export class KeycloakCredentialsApi implements ICredentialType {
    displayName = 'Keycloak API';
    name = 'keycloakCredentialsApi';
    documentationUrl = 'https://gitlab.com/n8n-nodes/keycloak';
    icon= 'file:keycloak.svg';
    properties: INodeProperties[] = [
        {
            displayName: 'Server URL',
            name: 'serverUrl',
            type: 'string',
            default: '',
            placeholder: 'https://your-keycloak-instance.com/',
            required: true,
            description: 'The URL of your Keycloak server. with / at the end',
        },
        {
            displayName: 'Default realm',
            name: 'defaultRealm',
            type: 'string',
            default: '',
            required: false,
            description: 'The default realm of your Keycloak server.',
        },
        {
            displayName: 'API Token',
            name: 'apiToken',
            type: 'string',
            typeOptions: { password: true },
            default: '',
            placeholder: 'Your Keycloak API Token',
            required: false,
            description: 'The API token for Keycloak.',
        },
        {
            displayName: 'Client ID',
            name: 'clientId',
            type: 'string',
            default: '',
            placeholder: 'Your Client ID',
            required: false,
            description: 'Client ID for obtaining the token.',
        },
        {
            displayName: 'Client Secret',
            name: 'clientSecret',
            type: 'string',
            typeOptions: { password: true },
            default: '',
            placeholder: 'Your Client Secret',
            required: false,
            description: 'Client Secret for obtaining the token.',
        },
    ];
}
